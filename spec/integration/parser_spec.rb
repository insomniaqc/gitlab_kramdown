require 'spec_helper'

context 'GitLab Kramdown Integration tests' do
  let(:options) do
    { input: 'GitlabKramdown', linkable_headers: false }
  end

  context 'HTML render' do
    Fixtures.text_files.each do |text_file|
      it "Renders #{File.basename(text_file)} as #{File.basename(Fixtures.html_file(text_file))}" do
        source = File.read(text_file)
        target = File.read(Fixtures.html_file(text_file))
        parser = Kramdown::Document.new(source, options)

        expect(parser.to_html).to eq(target)
      end
    end

    context 'linkable_headers enabled' do
      let(:options) do
        { input: 'GitlabKramdown', linkable_headers: true }
      end

      it 'Renders header as headers.linkable_headers.html' do
        text_file = File.join(Fixtures.fixtures_path, 'header.text')
        html_file = File.join(Fixtures.fixtures_path, 'header.linkable_headers.html')

        source = File.read(text_file)
        target = File.read(html_file)
        parser = Kramdown::Document.new(source, options)

        expect(parser.to_html).to eq(target)
      end
    end
  end
end
