#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
Bundler.setup
$LOAD_PATH.unshift File.join(File.dirname(__FILE__), '../lib')

require 'benchmark/ips'
require 'kramdown'
require 'gitlab_kramdown'

Benchmark.ips do |x|
  data = File.read(File.join(File.dirname(__FILE__), 'samples', 'pure-kramdown.md'))

  x.report("Kramdown #{Kramdown::VERSION}") { Kramdown::Document.new(data).to_html }
  x.report("GitLab Kramdown #{GitlabKramdown::VERSION}") do
    Kramdown::Document.new(data, input: 'GitlabKramdown', linkable_headers: false).to_html
  end

  x.compare!
end
