require 'rubygems'
require 'bundler'

begin
  Bundler.setup(:default, :development)
rescue Bundler::BundlerError => e
  $stderr.puts e.message
  $stderr.puts "Run `bundle install` to install missing gems"
  exit e.status_code
end

require 'rake'
require 'juwelier'

Juwelier::Tasks.new do |gem|
  # gem is a Gem::Specification... see http://guides.rubygems.org/specification-reference/ for more options
  gem.name = "gitlab_kramdown"
  gem.homepage = "http://gitlab.com/gitlab-org/gitlab_kramdown"
  gem.license = "MIT"
  gem.summary = %Q{GitLab Flavored Kramdown}
  gem.description = %Q{GitLab Flavored Markdown extensions on top of Kramdown markup. Tries to be as close as possible to existing extensions.}
  gem.email = "brodock@gmail.com"
  gem.authors = ["Gabriel Mazetto"]
  gem.required_ruby_version = '~> 2.4'
  gem.metadata = {
    'homepage_uri' => gem.homepage,
    'changelog_uri' => "#{gem.homepage}/blob/master/CHANGELOG.md",
    'source_code_uri' => gem.homepage,
    'bug_tracker_uri' => "#{gem.homepage}/issues"
  }
  gem.files = FileList['lib/**/*.rb']

  # dependencies defined in Gemfile
end
Juwelier::RubygemsDotOrgTasks.new

require 'rspec'
require 'rspec/core/rake_task'

desc "Run all examples"
RSpec::Core::RakeTask.new(:spec) do |t|
  t.ruby_opts = %w[-w]
  t.rspec_opts = %w[--color --format progress]
  t.rspec_opts += %w[--format RspecJunitFormatter --out rspec.xml] if ENV['GITLAB_CI']
end


desc "Code coverage detail"
task :simplecov do
  ENV['COVERAGE'] = "true"
  Rake::Task['spec'].execute
end

task :default => :test

require 'rdoc/task'
Rake::RDocTask.new do |rdoc|
  version = File.exist?('VERSION') ? File.read('VERSION') : ""

  rdoc.rdoc_dir = 'rdoc'
  rdoc.title = "gitlab_kramdown #{version}"
  rdoc.rdoc_files.include('README*')
  rdoc.rdoc_files.include('lib/**/*.rb')
end
