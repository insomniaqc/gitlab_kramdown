# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.2] - 2018-11-21
### Changed
- URL autolinks will not execute inside `[]()` or `[]` blocks
- Freezing `kramdown` version to allow only patchlevel changes

## [0.4.1] - 2018-11-02
### Added
- Codebase now have a benchmark utility to compare with plain Kramdown

### Changed
- Many performance improvements on existing RegExp rules
- Improve gemspec metadata and include only needed files on gem

## [0.4.0] - 2018-04-12
### Added
- URL auto-linking support

### Changed
- Fixed many inconsistencies in references autolink

## [0.3.0] - 2018-03-23
### Added
- Headers will include by default an anchor tag (you can disable with `linkable_headers: false`)

### Changed
- Fixed multiline blockquote delimiter
- GitLab URL is not customizable with `gitlab_url: 'http://yourcustomgitlab.com'`

## [0.2.0] - 2018-03-19
### Added
- Syntax highlighter uses ``` or ~~~

### Changed
- Requires Ruby 2.4
- Requires rouge `~> 3.0`

## [0.1.0] - 2018-03-17
### Added
- The initial version of the Gem
- Special GitLab References
- Multiline Blockquote

[0.4.2]: https://gitlab.com/gitlab-org/gitlab_kramdown/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/gitlab-org/gitlab_kramdown/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/gitlab-org/gitlab_kramdown/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/gitlab-org/gitlab_kramdown/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/gitlab-org/gitlab_kramdown/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/gitlab-org/gitlab_kramdown/compare/15b5e4b46aa0e42974ec2e5ee36c68d97219736f...v0.1.0
